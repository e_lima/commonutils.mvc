﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using CommonUtils.SpecificValidations;

namespace CommonUtils.MVC.Validation
{
    public class RG : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (!(value is string))
                return false;

            return value.IsValid<RGString>();
        }
    }
}
