﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CommonUtils.MVC.Validation
{
    public class CPFRegex : RegularExpressionAttribute
    {
        const string pattern = @"^\d{3}\.\d{3}\.\d{3}-\d{2}$";

        static CPFRegex()
        {
            DataAnnotationsModelValidatorProvider
                .RegisterAdapter(typeof(CPFRegex), typeof(RegularExpressionAttributeAdapter));
        }

        public CPFRegex() : base(pattern)
        {
        }
    }
}
