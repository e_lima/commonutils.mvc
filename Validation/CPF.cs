﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using CommonUtils.SpecificValidations;

namespace CommonUtils.MVC.Validation
{
    public class CPF : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (!(value is String))
                return false;

            return value.ToString()
                .Replace("-", String.Empty)
                .Replace(".", String.Empty)
                .IsValid<CpfString>();
        }
    }
}
