﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CommonUtils.MVC.Validation
{
    public class RGRegex : RegularExpressionAttribute
    {
        const string pattern = @"^\d{2}\.\d{3}\.\d{3}\-\w$";

        static RGRegex()
        {
            DataAnnotationsModelValidatorProvider
                .RegisterAdapter(typeof(RGRegex), typeof(RegularExpressionAttributeAdapter));
        }

        public RGRegex()
            : base(pattern)
        {
        }
    }
}
